package com.example.sportsworlddemo1.reservationpage;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sportsworlddemo1.Login;
import com.example.sportsworlddemo1.R;
import com.example.sportsworlddemo1.Register;
import com.example.sportsworlddemo1.homepage.Reservation1;
import com.example.sportsworlddemo1.database.ReservationDB;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Reservation4 extends AppCompatActivity{
    Button btn1,btn2;
    EditText studentName,studentId,equipment1,eNo1,date1,time1;

    TextView textViewResult;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reservation4);

        //在onCreate方法中初始化textViewResult
        textViewResult = findViewById(R.id.textViewResult);

        //輸入值
        Bundle bundle = getIntent().getExtras();
        String e1 = bundle.getString("姓名" );
        TextView result1 = (TextView)findViewById(R.id.UserIDText3);
        result1.setText(e1);

        String e2 = bundle.getString("學號" );
        TextView result2 = (TextView)findViewById(R.id.UserIDText5);
        result2.setText(e2);

        String e3 = bundle.getString("器材" );
        TextView result3 = (TextView)findViewById(R.id.UserIDText7);
        result3.setText(e3);
        String e4 = bundle.getString("編號" );
        TextView result4 = (TextView)findViewById(R.id.UserIDText9);
        result4.setText(e4);

        String d1 = bundle.getString("日期" );
        TextView result5 = (TextView)findViewById(R.id.UserIDText11);
        result5.setText(d1);

        if (bundle != null) {
            String mSpn = bundle.getString("spinner");
            String mSpn1 = bundle.getString("spinner1");
            TextView result6 = (TextView)findViewById(R.id.UserIDText2);
            result6.setText(mSpn + " ~ " + mSpn1);
        }
        //抓取資料
        studentName=findViewById(R.id.UserIDText3);
        studentId = findViewById(R.id.UserIDText5);
        equipment1 = findViewById(R.id.UserIDText7);
        eNo1 = findViewById(R.id.UserIDText9);
        date1 = findViewById(R.id.UserIDText11);
        time1 = findViewById(R.id.UserIDText2);

        btn2 = findViewById(R.id.Confirm);
        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String sname = studentName.getText().toString().trim();
                String sId = studentId.getText().toString().trim();
                String equipment = equipment1.getText().toString().trim();
                String eNo = eNo1.getText().toString().trim();
                String date = date1.getText().toString().trim();
                String time = time1.getText().toString().trim();

                ReservationDB reservationThread = new ReservationDB(sname, sId, equipment, eNo, date, time, textViewResult, Reservation4.this);
                Thread thread = new Thread(reservationThread);
                thread.start();

                Intent intent = new Intent();
                intent.setClass(Reservation4.this, Reservation1.class);
                startActivity(intent);
            }
        });

    }
}
