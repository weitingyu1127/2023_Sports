package com.example.sportsworlddemo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sportsworlddemo1.database.LoginDB;
import com.example.sportsworlddemo1.database.RegisterDB;
import com.example.sportsworlddemo1.homepage.Running;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Register extends AppCompatActivity {
    EditText editUsername, editPassword, editPhone, editEmail,editHeight,editWeight;
    Button buttonRegister;
    TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        editUsername = findViewById(R.id.editUsername);
        editPassword = findViewById(R.id.editPassword);
        editPhone = findViewById(R.id.editPhone);
        editEmail = findViewById(R.id.editEmail);
        editHeight = findViewById(R.id.editHeight);
        editWeight = findViewById(R.id.editWeight);

        buttonRegister = findViewById(R.id.buttonRegister);
        textViewResult = findViewById(R.id.textViewResult);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = editUsername.getText().toString().trim();
                String password = editPassword.getText().toString().trim();
                String email = editEmail.getText().toString().trim();
                String phone = editPhone.getText().toString().trim();
                String height = editHeight.getText().toString().trim();
                String weight = editWeight.getText().toString().trim();


                if (username.isEmpty() || password.isEmpty() || email.isEmpty() || phone.isEmpty() || height.isEmpty() || weight.isEmpty())  {
                    textViewResult.setText("請填寫完整資訊");
                    return;
                }

                Handler handler = new Handler();

                // Create a LoginThread instance
                RegisterDB registerThread = new RegisterDB(editUsername, editPassword, editPhone, editEmail,
                        editHeight, editWeight, textViewResult, handler, Register.this);

                // Start the login thread
                Thread thread = new Thread(registerThread);
                thread.start();

                Intent intent = new Intent(Register.this, Login.class);
                startActivity(intent);
            }
        });
    }
}
