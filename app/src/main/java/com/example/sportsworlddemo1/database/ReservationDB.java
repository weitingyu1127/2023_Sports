package com.example.sportsworlddemo1.database;
import android.os.Handler;
import android.widget.TextView;

import com.example.sportsworlddemo1.homepage.Reservation1;
import com.example.sportsworlddemo1.reservationpage.Reservation4;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

public class ReservationDB implements Runnable {
    private String sname;
    private String sId;
    private String equipment;
    private String eNo;
    private String date;
    private String time;
    private TextView textViewResult;
    private Reservation4 activity;

    public ReservationDB(String sname, String sId, String equipment, String eNo, String date, String time,
                         TextView textViewResult, Reservation4 activity) {
        this.sname = sname;
        this.sId = sId;
        this.equipment = equipment;
        this.eNo = eNo;
        this.date = date;
        this.time = time;
        this.textViewResult = textViewResult;
        this.activity = activity;
    }

    @Override
    public void run() {
        try {
            URL url = new URL("http://10.0.2.2/reservationdemo1.php");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.connect();

            String data = "sname=" + URLEncoder.encode(sname, "UTF-8")
                    + "&sId=" + URLEncoder.encode(sId, "UTF-8")
                    + "&equipment=" + URLEncoder.encode(equipment, "UTF-8")
                    + "&eNo=" + URLEncoder.encode(eNo, "UTF-8")
                    + "&date=" + URLEncoder.encode(date, "UTF-8")
                    + "&time=" + URLEncoder.encode(time, "UTF-8");

            connection.getOutputStream().write(data.getBytes());

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
                String box = "";
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    box += line + "\n";
                }
                inputStream.close();
                final String result = box;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textViewResult.setText(result);
                        if (result.trim().equals("預約登記完成")) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                }
                            }, 3000);
                        }
                    }
                });
            } else {
                throw new Exception("HTTP response code: " + responseCode);
            }
        } catch (Exception e) {
            final String error = e.toString();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textViewResult.setText(error);
                }
            });
        }
    }

}

