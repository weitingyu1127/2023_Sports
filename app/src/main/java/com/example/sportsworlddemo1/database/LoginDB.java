package com.example.sportsworlddemo1.database;
// LoginThread.java

import android.os.Handler;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sportsworlddemo1.Login;
import com.example.sportsworlddemo1.reservationpage.Reservation4;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class LoginDB implements Runnable {
    private EditText editUsername;
    private EditText editPassword;
    private TextView textViewResult;
    private Handler handler;
    private Login activity;
    public LoginDB(EditText editUsername, EditText editPassword, TextView textViewResult, Handler handler, Login activity) {
        this.editUsername = editUsername;
        this.editPassword = editPassword;
        this.textViewResult = textViewResult;
        this.handler = handler;
        this.activity=activity;
    }

    @Override
    public void run() {
        try {
            String username = editUsername.getText().toString().trim();
            String password = editPassword.getText().toString().trim();

            // Connect to the login PHP script
            URL url = new URL("http://10.0.2.2/logindemo1.php");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.connect();

            // Prepare the login data
            String data = "username=" + URLEncoder.encode(username, "UTF-8")
                    + "&password=" + URLEncoder.encode(password, "UTF-8");

            // Send the login data
            connection.getOutputStream().write(data.getBytes());

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
                String box = "";
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    box += line + "\n";
                }
                inputStream.close();
                final String result = box;

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        textViewResult.setText(result);
                        if (result.trim().equals("登入成功 3秒後切換進入主頁")) {
                            // Login successful, switch to the main page after 3 seconds
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                }
                            }, 3000);
                        }
                    }
                });
            } else {
                throw new Exception("HTTP response code: " + responseCode);
            }

        } catch (Exception e) {
            final String error = e.toString();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    textViewResult.setText(error);
                }
            });
        }
    }
}
