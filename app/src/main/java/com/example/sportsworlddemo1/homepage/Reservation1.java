package com.example.sportsworlddemo1.homepage;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.sportsworlddemo1.R;
import com.example.sportsworlddemo1.reservationpage.Reservation2;

public class Reservation1 extends AppCompatActivity {
    ImageButton Ibtn1,Ibtn2,Ibtn3,Ibtn4,Ibtn5;
    Button btn1,btn2;
    TextView textViewResult;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reservation1);
        textViewResult = findViewById(R.id.textViewResult);

        //取得按鍵
        btn1 = findViewById(R.id.revise1);
        btn1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(Reservation1.this, Reservation2.class);
                startActivity(intent);
            }
        });
        btn2 = findViewById(R.id.reservation);
        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(Reservation1.this, Reservation2.class);
                startActivity(intent);
            }
        });


        Ibtn3 = findViewById(R.id.btn_home);
        Ibtn3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(Reservation1.this, Running.class);
                startActivity(intent);
            }
        });


        Ibtn4 = findViewById(R.id.btn_training);
        Ibtn4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(Reservation1.this, Training.class);
                startActivity(intent);
            }
        });

        Ibtn5 = findViewById(R.id.btn_article);
        Ibtn5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(Reservation1.this, Article_food.class);
                startActivity(intent);
            }
        });
    }
}
