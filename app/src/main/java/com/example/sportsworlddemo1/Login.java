package com.example.sportsworlddemo1;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.example.sportsworlddemo1.database.LoginDB;
import com.example.sportsworlddemo1.homepage.Reservation1;
import com.example.sportsworlddemo1.homepage.Running;
import com.example.sportsworlddemo1.reservationpage.Reservation4;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Login extends AppCompatActivity{
    EditText editUsername, editPassword;
    Button buttonLogin;
    TextView textViewResult;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Button nextPageLogin = findViewById(R.id.buttonLogin);
        Button nextPageRegister = findViewById(R.id.buttonregister);

        nextPageRegister.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(Login.this, Register.class);
            startActivity(intent);
        });


        editUsername = findViewById(R.id.l_editUsername);
        editPassword = findViewById(R.id.l_editPassword);
        buttonLogin = findViewById(R.id.buttonLogin);
        textViewResult = findViewById(R.id.textViewResult);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();

                // Create a LoginThread instance
                LoginDB loginThread = new LoginDB(editUsername, editPassword, textViewResult, handler,Login.this);

                // Start the login thread
                Thread thread = new Thread(loginThread);
                thread.start();

                Intent intent = new Intent(Login.this, Running.class);
                startActivity(intent);
            }
        });
    }
}
